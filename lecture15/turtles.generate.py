#!/usr/bin/env python3

import random

for _ in range(1000):
    strength = random.randint(0, 1000)
    weight   = random.randint(0, strength)
    print(weight, strength)
