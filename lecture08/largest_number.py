#!/usr/bin/env python3

import functools
import sys

# Functions

def compare_numbers(a, b):
    ab = int(a + b)
    ba = int(b + a)
    return ab - ba

# Main execution

if __name__ == '__main__':
    for line in sys.stdin:
        print(''.join(sorted(line.split(), key=functools.cmp_to_key(compare_numbers), reverse=True)))
