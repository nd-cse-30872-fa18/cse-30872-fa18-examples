/* sqrt.c: computes sqrt of command-line arguments */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc < 2) {
    	fprintf(stderr, "Usage: %s numbers...\n", argv[0]);
    	return EXIT_FAILURE;
    }

    for (int i = 1; i < argc; i++) {
    	double n = strtod(argv[i], NULL);
    	printf("The square root of %lf is %lf\n", n, sqrt(n));
    }

    return EXIT_SUCCESS;
}
