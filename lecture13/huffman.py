#!/usr/bin/env python3

import collections
import heapq
import sys

# Node Class

class Node(object):
    def __init__(self, name, value, left=None, right=None):
        self.left  = left
        self.right = right
        self.name  = name
        self.value = value

    def walk(self, codeword, results):
        if self.left:   # Recurse left
            self.left.walk(codeword + '0', results)

        if self.right:  # Recurse right
            self.right.walk(codeword + '1', results)

        # Base: leaf node, so add codeword
        if not self.left and not self.right:
            results[self.name] = codeword

    def __lt__(self, other):
        if self.value == other.value:
            return len(self.name) < len(other.name)
        return self.value < other.value

    def __str__(self):
        return 'name={}, value={}'.format(self.name, self.value)

# Main execution

if __name__ == '__main__':
    for line in sys.stdin:
        counter = collections.Counter(line.strip())     # Compute histograms
        nodes   = [Node(*p) for p in counter.items()]   # List of nodes

        # Greedily combine largest nodes
        heapq.heapify(nodes)                            # Heapify nodes
        while len(nodes) > 1:
            left  = heapq.heappop(nodes)                # Pop two largest
            right = heapq.heappop(nodes)
            node  = Node(left.name + right.name, left.value + right.value, left, right)
            heapq.heappush(nodes, node)                 # Push new meta-node

        # Walk from root to generate codewords
        results = {}
        nodes[0].walk('', results)
        print(', '.join('{} = {}'.format(k, v) for k, v in sorted(results.items())))
