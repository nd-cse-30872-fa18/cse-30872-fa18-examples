#!/bin/bash

# Input

test-input() {
    cat <<EOF
0 1
0 1 0
0 1 1 0
0 1 0 1
0 0 0 0
EOF
}

# Main execution

if [ $# -lt 1 ]; then
    echo "Usage: $0 PROGRAM"
    exit 1
fi

PROGRAM=$1

echo -n "Testing $PROGRAM ... "
#test-input | { time $PROGRAM >& /dev/null; } |& awk '/real/ { print $2 }'
if test-input | timeout 5 $PROGRAM >& /dev/null; then
    echo "Success"
else
    echo "Failure"
fi
