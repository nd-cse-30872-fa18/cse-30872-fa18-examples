#!/usr/bin/env python3

import contiguous_array
import unittest

# Test Case

class CATestCase(unittest.TestCase):

    def test_00(self):
        result = contiguous_array.find_maximum_contiguous_array([0, 1])
        self.assertEqual(result, 2)
    
    def test_01(self):
        result = contiguous_array.find_maximum_contiguous_array([1, 0])
        self.assertEqual(result, 2)
    
    def test_02(self):
        result = contiguous_array.find_maximum_contiguous_array([0, 1, 1, 0])
        self.assertEqual(result, 4)
    
    def test_03(self):
        result = contiguous_array.find_maximum_contiguous_array([0, 0, 0, 0])
        self.assertEqual(result, 0)

# Main execution

if __name__ == '__main__':
    unittest.main()
