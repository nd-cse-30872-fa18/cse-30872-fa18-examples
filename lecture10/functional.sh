#!/bin/bash

# Input

test-input() {
    cat <<EOF
0 1
0 1 0
0 1 1 0
0 1 0 1
0 0 0 0
EOF
}

# Output 

test-output() {
    cat <<EOF
2
2
4
4
0
EOF
}

# Main execution

if [ $# -lt 1 ]; then
    echo "Usage: $0 PROGRAM"
    exit 1
fi

PROGRAM=$1

echo -n "Testing $PROGRAM ... "
if diff -q <(test-input | $PROGRAM) <(test-output) >& /dev/null; then
    echo "Success"
else
    echo "Failure"
fi
