#!/usr/bin/env python3

import collections
import sys

# Functions

def find_maximum_contiguous_array(array):
    '''
    >>> find_maximum_contiguous_array([0, 1])
    2
    >>> find_maximum_contiguous_array([0, 1, 0])
    2
    >>> find_maximum_contiguous_array([0, 1, 1, 0])
    4
    >>> find_maximum_contiguous_array([0, 0, 0, 0])
    0
    '''
    totals     = {0: 0} # Totals -> Index where we first achieved this total
    cur_total  = 0      # Current total
    max_length = 0      # Maximum length

    for index, digit in enumerate(array, 1):
        cur_total += 1 if digit == 1 else -1
        if cur_total in totals:
            max_length = max(max_length, index - totals[cur_total])
        else:
            totals[cur_total] = index

    return max_length
        
# Main execution

if __name__ == '__main__':
    for line in sys.stdin:
        digits = map(int, line.split())
        print(find_maximum_contiguous_array(digits))
