#!/usr/bin/env python3

# Globals

Fibonacci = {}

# Functions

def fibonacci(n):
    if n == 0:              # Base case
        return 0
    elif n <= 2:            # Base case
        return 1

    if n not in Fibonacci:  # Memoize recursive step
        Fibonacci[n] = fibonacci(n - 1) + fibonacci(n - 2) 

    return Fibonacci[n]

# Main execution

if __name__ == '__main__':
    for i in range(100):
        print('fibonacci({}) = {}'.format(i, fibonacci(i)))
