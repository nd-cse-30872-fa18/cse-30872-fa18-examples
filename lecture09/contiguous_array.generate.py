#!/usr/bin/env python2.7

import random

print '0'
print '1'
print '0 0'
print '0 1'
print '1 0'
print '1 1'
print '0 0 0'
print '0 0 1'
print '0 1 0'
print '0 1 1'
print '1 0 0'
print '1 0 1'
print '1 1 0'
print '1 1 1'

for x in range(1,4):
    length = 10 ** x
    print ' '.join(map(str, (random.randint(0, 1) for _ in range(length))))
