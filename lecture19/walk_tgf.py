#!/usr/bin/env python3

import collections
import sys

# Graph structure

Graph = collections.namedtuple('Graph', 'labels edges')

# Read Graph

def read_graph():
    ''' Construct adjacency set '''
    labels = {}
    edges  = collections.defaultdict(set)

    # Read labels
    for line in sys.stdin:
        if line.startswith('#'):
            break

        node, label = line.split()
        labels[int(node)] = label

    # Read edges
    for line in sys.stdin:
        fr, to = map(int, line.split())
        edges[fr].add(to)

    return Graph(labels, edges)

# Walk graph

def walk_graph_dfs_rec(graph, root, visited=None):
    visited = visited or set()

    if root in visited:
        return

    visited.add(root)

    print(root)

    for neighbor in graph.edges[root]:
        walk_graph_dfs_rec(graph, neighbor, visited)

def walk_graph_dfs_iter(graph, root):
    frontier = [root]
    visited  = set()

    while frontier:
        node = frontier.pop()

        if node in visited:
            continue
    
        visited.add(node)

        print(node)

        for neighbor in reversed(list(graph.edges[node])):
            frontier.append(neighbor)

def walk_graph_bfs_iter(graph, root):
    frontier = [root]
    visited  = set()

    while frontier:
        node = frontier.pop(0)

        if node in visited:
            continue
    
        visited.add(node)

        print(node)

        for neighbor in graph.edges[node]:
            frontier.append(neighbor)

GraphWalkers = (
    walk_graph_dfs_rec,
    walk_graph_dfs_iter,
    walk_graph_bfs_iter,
)

# Main execution

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: {} root [0 = DFS_REC | 1 = DFS_ITER | 2 = BFS_ITER]'.format(sys.argv[0]))
        sys.exit(1)

    graph = read_graph()
    root  = int(sys.argv[1])
    walk  = int(sys.argv[2])

    GraphWalkers[walk](graph, root)
