// permutations.cpp

#include <iostream>
#include <set>
#include <vector>

using namespace std;

// Functions

void permutations(vector<int> &p, set<int> &c) {
    // Display permutation at each function call
    for (auto e : p) { cout << e << " "; }; cout << endl;

    if (c.empty()) {	// Base: Used all candidates, so we are done
    	return;
    }

    for (auto e : c) {
	p.push_back(e);	// Place one element into current permutation
	c.erase(e);	// Remove element from candiate list

	// Recurse on sublist (which now is one item shorter and no longer
	// includes the previously placed element as a candidate
	permutations(p, c);

	c.insert(e);	// Add element back to candidate set
	p.pop_back();	// Remove element from current permutation
    }
}

// Main execution

int main(int argc, char *argv[]) {
    vector<int> p = {};		// Permutation
    set<int>	c = {0, 1, 2};	// Set of candidates

    permutations(p, c);
    return 0;
}
